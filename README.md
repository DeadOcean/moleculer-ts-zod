# Zodeculer - A type-safe MoleculerJS microservice template

This is a strongly typed [MoleculerJS](https://github.com/moleculerjs/moleculer)
microservice template, taking advantage of [zod](https://github.com/colinhacks/zod)'s
runtime validations and type inference. It also has a convenient MongoDB mixin that
allows for fully type-safe queries to your database.

## Project structure

```
src/
├─ actions/             <- add your actions here
├─ entities/            <- your zod schemas for your entities go in here
├─ lib/                 <- some of the magic comes from here!
├─ mixins/              <- the database magic comes from here!
├─ broker.ts            <- the microservice's entry point
├─ config.ts            <- self-explanatory
├─ service.ts           <- this is where you plug your actions into your service
├─ types.ts             <- typings
test/
├─ dbHelper.ts          <- don't delete this!

```

The `src/lib/` directory has two special files which you must not delete: `validator.ts`
and `zodAction.ts`, these two are responsible for the validation of every action's request
and response, don't delete them!

The `src/mixins/` folder contains the database connection mixin, which adds the following
properties to the action's `this`:

- `client`: the MongoDB Client instance, uses the native Node.js MongoDB driver
- `db`: the database instance, which is set by the `MONGO_DEFAULT_DATABASE` environment variable
- `collection`: a handy shortcut for the default collection, set as an option at the moment of
  plugging the mixin into the service schema.

## Adding actions

Before adding an action, you'll need to overwrite the `ZodService` type from `src/types.ts`,
pass it your default collection's interface

To add an action to the service, simply create a new file inside `src/actions/` and add the
following:

```ts
// src/actions/someAction.ts
import { z } from 'zod';
import type { SomeService } from '@/types'; // the example uses PostService
import { zodAction } from '@/lib/zodAction';

export const someAction = zodAction({
  rest: 'GET /',
  params: z.object({ name: z.string() }),
  response: z.object({ message: z.string() }),
  async handler(this: SomeService, ctx) {
    const { name } = ctx.params; // type of name is string!

    return {
      message: `Hello, ${name}!`,
    }; // response is also fully typed and validated at runtime
  },
});
```

Then, import your brand new action into `src/service.ts` and plug it into `baseServiceSchema.actions`:

```ts
import type { ServiceSchema } from 'moleculer';
import { someAction } from '@/actions/someAction';

export const baseServiceSchema = {
  name: 'service' // or whatever your service is called,
    actions: {
      someAction,
    },
  },
} satisfies ServiceSchema;
```

And that's it!

## Development environment

### Pre-requisites

OS dependencies:

- Node.js >= 18.16
- PNPM >= 8

```
# install dependencies
pnpm install

# Start server in development mode
pnpm dev
```

## Running tests

Run tests in watch mode to get instant feedback (powered by [vitest](https://vitest.dev)):

```
pnpm test
```

To run tests with coverage reporting locally:

```
pnpm test:coverage
```

## Linting, formatting and typechecking

This project uses pre-commit hooks (powered by [husky](https://typicode.github.io/husky/)),
so it's not necessary to run these manually:

```
pnpm lint
pnpm format
```

This project uses [tsx](https://github.com/esbuild-kit/tsx) as its runtime, so transpiling
to JS is not necessary. `tsc` is only used for type-checking (both in CI and in your IDE as
you code), but can be run manually:

```
pnpm typecheck
```

TODO: add instructions for replacing the default collection schema.
