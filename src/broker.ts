import { ServiceBroker, type ServiceSchema } from 'moleculer';

import { options } from '@/config.ts';
import { mongoDbMixin } from '@/mixins/db.ts';
import { baseServiceSchema } from '@/service.ts';

const broker = new ServiceBroker(options);

const serviceSchema = {
  ...baseServiceSchema,
  mixins: [
    mongoDbMixin({
      uri: process.env.MONGO_URI,
      defaultCollection: 'posts',
      options: {
        authSource: 'admin',
        authMechanism: 'DEFAULT',
      },
    }),
  ],
} satisfies ServiceSchema;

broker.createService(serviceSchema);

broker
  .start()
  .then(() => broker.logger.info(`Service ${options.nodeID} started!`))
  .catch((e) => {
    broker.logger.error(`Broker error!`, e);
  });
