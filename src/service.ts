import type { ServiceSchema } from 'moleculer';

import { create } from '@/actions/create.ts';
import { createMany } from '@/actions/createMany.ts';
import { get } from '@/actions/get.ts';
import { update } from '@/actions/update.ts';
import { find } from '@/actions/find.ts';

export const baseServiceSchema = {
  name: 'posts',
  actions: {
    create,
    update,
    createMany,
    get,
    find,
  },
} satisfies ServiceSchema;
