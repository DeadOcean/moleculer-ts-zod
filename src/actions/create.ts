import { z } from 'zod';

import { postSchema } from '@/entities/post.ts';
import { zodAction } from '@/lib/zodAction.ts';
import type { PostsService } from '@/types.ts';
import { ZodObjectId } from '@/lib/schemas.ts';

const response = postSchema.merge(z.object({ _id: ZodObjectId }));

export const create = zodAction({
  rest: 'POST /',
  params: postSchema,
  response,
  async handler(this: PostsService, ctx) {
    const { insertedId } = await this.collection.insertOne(ctx.params);
    const post = (await this.collection.findOne({ _id: insertedId }))!;

    return post;
  },
});
