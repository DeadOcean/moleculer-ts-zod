import { Errors } from 'moleculer';
import { z } from 'zod';

import { postSchema } from '@/entities/post.ts';
import { zodAction } from '@/lib/zodAction.ts';
import type { PostsService } from '@/types.ts';
import { ZodObjectId } from '@/lib/schemas.ts';

const paramsSchema = z.object({ _id: ZodObjectId });
const responseSchema = postSchema.merge(paramsSchema);

export const get = zodAction({
  rest: 'GET /:_id',
  params: paramsSchema,
  response: responseSchema,
  async handler(this: PostsService, ctx) {
    const post = await this.collection.findOne({ _id: ctx.params._id });

    if (!post) {
      throw new Errors.MoleculerClientError(
        'Post not found!',
        404,
        'postNotFound'
      );
    }

    return post;
  },
});
