import { Errors } from 'moleculer';
import z from 'zod';

import { postSchema } from '@/entities/post.ts';
import { zodAction } from '@/lib/zodAction.ts';
import type { PostsService } from '@/types.ts';
import { ZodObjectId } from '@/lib/schemas.ts';

const paramsSchema = postSchema.partial().merge(z.object({ _id: ZodObjectId }));
const responseSchema = postSchema.merge(z.object({ _id: ZodObjectId }));

export const update = zodAction({
  rest: 'PUT /:_id',
  params: paramsSchema,
  response: responseSchema,
  async handler(this: PostsService, ctx) {
    const { _id, ...updateParams } = ctx.params;
    const post = await this.collection.findOne({ _id: ctx.params._id });

    if (!post) {
      throw new Errors.MoleculerClientError(
        'Post not found!',
        404,
        'postNotFound'
      );
    }

    const updated = await this.collection.findOneAndUpdate(
      { _id },
      { $set: updateParams },
      { returnDocument: 'after' }
    );

    return updated.value!;
  },
});
