import { z } from 'zod';

import { postSchema } from '@/entities/post.ts';
import { zodAction } from '@/lib/zodAction.ts';
import type { PostsService } from '@/types.ts';
import { ZodObjectId } from '@/lib/schemas.ts';

const params = z.object({ data: z.array(postSchema) });

const response = z.array(postSchema.merge(z.object({ _id: ZodObjectId })));

export const createMany = zodAction({
  rest: 'POST /bulk',
  params,
  response,
  async handler(this: PostsService, ctx) {
    const { insertedIds } = await this.collection.insertMany(ctx.params.data);

    return this.collection
      .find({ _id: { $in: Object.values(insertedIds) } })
      .toArray();
  },
});
