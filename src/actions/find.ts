import { z } from 'zod';

import { postSchema } from '@/entities/post.ts';
import { zodAction } from '@/lib/zodAction.ts';
import type { PostsService } from '@/types.ts';
import { ZodObjectId } from '@/lib/schemas.ts';
import { getProjectionFromParams } from '@/lib/func.ts';

const paramsSchema = z.object({
  fields: z.array(postSchema.merge(z.object({ _id: ZodObjectId })).keyof()),
});

const responseSchema = z.array(
  postSchema.merge(z.object({ _id: ZodObjectId })).partial()
);

export const find = zodAction({
  rest: 'GET /',
  params: paramsSchema,
  response: responseSchema,
  handler(this: PostsService, ctx) {
    return this.collection
      .find({}, { projection: getProjectionFromParams(ctx.params.fields) })
      .toArray();
  },
});
