import type { BrokerOptions, Validator } from 'moleculer';

import { ZodValidator } from '@/lib/validator.ts';
import { getUniqId } from '@/lib/func.ts';

const pinoOptions = {
  development: {
    level: 'debug',
    transport: {
      target: 'pino-pretty',
      options: {
        translateTime: 'HH:MM:ss Z',
        ignore: 'pid,hostname,ns,mod',
      },
    },
  },
  production: {
    level: process.env.LOG_LEVEL ?? 'info',
  },
  test: {
    level: 'silent',
  },
} as const;

export const options = {
  namespace: getUniqId(),
  nodeID: 'posts',
  logger: {
    type: 'Pino',
    options: {
      pino: {
        options: pinoOptions[process.env.NODE_ENV],
      },
    },
  },
  cacher: false,
  transporter: {
    type: 'NATS',
    options: {
      maxPacketSize: 50 * 1024 * 1024,
      url: process.env.NATS_URI,
    },
  },
  validator: new ZodValidator() as unknown as Validator,
  tracking: {
    enabled: true,
    shutdownTimeout: 10 * 1000,
  },
} satisfies BrokerOptions;
