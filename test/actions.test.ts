import { faker } from '@faker-js/faker';
import type { Validator } from 'moleculer';
import { ServiceBroker } from 'moleculer';
import { ObjectId } from 'mongodb';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { afterAll, afterEach, beforeAll, describe, expect, test } from 'vitest';

import type { create } from '@/actions/create.ts';
import type { createMany } from '@/actions/createMany.ts';
import type { update } from '@/actions/update.ts';
import type { PostSchema } from '@/entities/post.ts';
import { ZodValidator } from '@/lib/validator.ts';
import { mongoDbMixin } from '@/mixins/db.ts';
import { baseServiceSchema } from '@/service.ts';
import type { CallParams, PostsService } from '@/types.ts';
import { getDbHelper } from './dbHelper.ts';

describe('Test Posts CRUD actions', async () => {
  const broker = new ServiceBroker({
    logger: false,
    validator: new ZodValidator() as unknown as Validator,
  });

  const mongod = await MongoMemoryServer.create();

  const serviceSchema = {
    ...baseServiceSchema,
    mixins: [
      mongoDbMixin({
        uri: mongod.getUri(),
        defaultCollection: 'posts',
      }),
    ],
  };

  const service = broker.createService(serviceSchema) as PostsService;
  const dbHelper = getDbHelper(service.client, mongod);

  beforeAll(() => broker.start());
  afterEach(() => dbHelper.clearDatabase());
  afterAll(async () => {
    await dbHelper.closeDatabase();
    await broker.stop();
  });

  test('"create" action', async () => {
    const res = await broker.call<
      ReturnType<(typeof create)['handler']>,
      CallParams<(typeof create)['params']>
    >('posts.create', {
      title: faker.commerce.productName(),
      content: faker.lorem.sentence(),
      author: faker.database.mongodbObjectId(),
    });

    expect(res).toBeDefined();
  });

  test('"createMany" action', async () => {
    const res = await broker.call<
      ReturnType<(typeof createMany)['handler']>,
      CallParams<(typeof createMany)['params']>
    >('posts.createMany', {
      data: Array.from({ length: 100 }, () => ({
        title: faker.commerce.productName(),
        content: faker.lorem.sentence(),
        author: faker.database.mongodbObjectId(),
      })),
    });

    expect(res).toBeDefined();
  });

  test('"update" action', async () => {
    const postForInsert: PostSchema = {
      title: faker.commerce.productName(),
      content: faker.lorem.sentence(),
      author: new ObjectId(),
      active: true,
      createdAt: new Date(),
    };

    const { insertedId } = await service.collection.insertOne(postForInsert);

    const res = await broker.call<
      ReturnType<(typeof update)['handler']>,
      CallParams<(typeof update)['params']>
    >('posts.update', {
      _id: insertedId,
      title: faker.commerce.productName(),
    });

    expect(res.title !== postForInsert.title).toBe(true);
  });
});
