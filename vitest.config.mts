import { defineConfig, type UserConfig } from 'vitest/config';
import tsConfigPaths from 'vite-tsconfig-paths';

const reporters: NonNullable<UserConfig['test']>['reporters'] = ['default'];

if (!(process.env.FROM_HUSKY === '1')) {
  reporters.push('junit');
}

export default defineConfig({
  plugins: [tsConfigPaths()],
  test: {
    coverage: {
      provider: 'v8',
      reporter: ['text', 'cobertura'],
    },
    reporters,
    outputFile: './coverage/junit.xml',
    setupFiles: ['dotenv/config'],
  },
});
